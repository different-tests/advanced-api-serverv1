//go:build wireinject
// +build wireinject

package wire

import (
	"advanced-api-serverv1/internal/handler"
	"advanced-api-serverv1/internal/repository"
	"advanced-api-serverv1/internal/server"
	"advanced-api-serverv1/internal/service"
	"advanced-api-serverv1/pkg/app"
	"advanced-api-serverv1/pkg/helper/sid"
	"advanced-api-serverv1/pkg/jwt"
	"advanced-api-serverv1/pkg/log"
	"advanced-api-serverv1/pkg/server/http"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var repositorySet = wire.NewSet(
	repository.NewDB,
	repository.NewRedis,
	repository.NewRepository,
	repository.NewTransaction,
	repository.NewUserRepository,
)

var serviceSet = wire.NewSet(
	service.NewService,
	service.NewUserService,
)

var handlerSet = wire.NewSet(
	handler.NewHandler,
	handler.NewUserHandler,
)

var serverSet = wire.NewSet(
	server.NewHTTPServer,
	server.NewJob,
	server.NewTask,
)

// build App
func newApp(httpServer *http.Server, job *server.Job) *app.App {
	return app.NewApp(
		app.WithServer(httpServer, job),
		app.WithName("demo-server"),
	)
}

func NewWire(*viper.Viper, *log.Logger) (*app.App, func(), error) {

	panic(wire.Build(
		repositorySet,
		serviceSet,
		handlerSet,
		serverSet,
		sid.NewSid,
		jwt.NewJwt,
		newApp,
	))
}
