package service

import (
	"advanced-api-serverv1/internal/repository"
	"advanced-api-serverv1/pkg/helper/sid"
	"advanced-api-serverv1/pkg/jwt"
	"advanced-api-serverv1/pkg/log"
)

type Service struct {
	logger *log.Logger
	sid    *sid.Sid
	jwt    *jwt.JWT
	tm     repository.Transaction
}

func NewService(tm repository.Transaction, logger *log.Logger, sid *sid.Sid, jwt *jwt.JWT) *Service {
	return &Service{
		logger: logger,
		sid:    sid,
		jwt:    jwt,
		tm:     tm,
	}
}
